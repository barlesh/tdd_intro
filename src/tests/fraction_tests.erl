%%%-------------------------------------------------------------------
%%% @author root
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 31. Mar 2017 20:11
%%%-------------------------------------------------------------------
-module(fraction_tests).
-author("root").

-include_lib("eunit/include/eunit.hrl").

simple_test() ->
  ?assert(true).

add_test() ->
  % test to handle fraction with 0 at denuminator
  ?assertEqual(divide_by_zero, fraction:add({2,0},{1,1})),
  ?assertEqual(divide_by_zero, fraction:add({2,7},{5,0})),
  ?assertEqual(divide_by_zero, fraction:add({2,0},{1,0})),
  ?assertEqual(0, fraction:add(0,0)),
  ?assertEqual(2, fraction:add(1,1)),
  ?assertEqual(0, fraction:add(1,-1)),
  ?assertEqual(-3, fraction:add(1,-4)),
  ?assertEqual({3,2}, fraction:add(1,{1,2})),
  ?assertEqual({-1,2}, fraction:add(1,{-3,2})),
  ?assertEqual({-1,2}, fraction:add(-1,{1,2})),
  ?assertEqual(-1, fraction:add({1,2},{-3,2})),
  ?assertEqual(1, fraction:add({-1,2},{3,2})),
  ?assertEqual(-2, fraction:add({-1,2},{-3,2})),
  ?assertEqual({-9,2}, fraction:add({-6,2},{-3,2})),
  ?assertEqual({-7,2}, fraction:add({-6,3},{-3,2})),
  ?assertEqual({-7,2}, fraction:add({6,-3},{3,-2})),
  ?assertEqual({7,-2}, fraction:add({6,-3},{-3,2})),
  ?assertEqual({1,2}, fraction:add({-6,-3},{3,-2})),
  ?assertEqual({5,3}, fraction:add({2,3},1)),
  ?assertEqual({47,15}, fraction:add({7,3},{4,5})),
  ?assertEqual(1, fraction:add({2,3},{1,3})),
  ?assertEqual({1,3}, fraction:add(0,{1,3})),
  ?assertEqual({5,6}, fraction:add({2,4},{1,3})).
