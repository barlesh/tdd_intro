%%%-------------------------------------------------------------------
%%% @author root
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 31. Mar 2017 20:13
%%%-------------------------------------------------------------------
-module(fraction).
-author("root").

%% API
-export([add/2]).


%add(X,Y) when X == 0 -> Y;
%add(X,Y) when Y == 0 -> X;
add({_, X_DENOM},{_, Y_DENOM}) when X_DENOM == 0 orelse Y_DENOM == 0  -> divide_by_zero;
add({X_NUM, X_DENOM},{Y_NUM, Y_DENOM}) -> reduction({ X_NUM*Y_DENOM + Y_NUM*X_DENOM ,X_DENOM*Y_DENOM });
add(X,Y) when is_integer(X) andalso is_integer(Y) -> X+Y;
add({_X_NUM, X_DENOM} = X,Y) when  is_integer(Y) -> add(X, {Y*X_DENOM, max(abs(Y*X_DENOM),1)});
add(X,{_Y_NUM, Y_DENOM} = Y) when is_integer(X) -> add({X*Y_DENOM, max(1,abs(X*Y_DENOM))},Y).

%add(X,{Y_NUM, Y_DENOM}) when is_integer(X) -> X_NUM = X*Y_DENOM, X_DENOM=X*Y_DENOM, { X_NUM*Y_DENOM + Y_NUM*X_DENOM ,X_DENOM*Y_DENOM }.


gcd(N1,N2)->
      Number1 = abs(N1), Number2 = abs(N2),
      if
        Number1>Number2 ->
        if
         (Number2 == Number1 - Number2) -> Number2;
          true ->
            gcd(Number2,(Number1 - Number2))
        end;
      Number2>Number1 ->
         if
          (Number1 == Number2 - Number1) ->
              Number1;
          true ->
            gcd(Number1,(Number2 - Number1))
        end;
      true ->
       Number1
    end.

% if NUMINATOR is multiply of DENUMINATOR ( also, remind of NUM and DENUM is 0 )  then fractions can be turn to integer
reduction2({NUM,DENUM}) when NUM rem DENUM == 0 -> round(NUM/DENUM);
reduction2(Frac)->Frac.

% calculate gcd, then divide numinator and denuminator by it (and round results)
reduction( {NUMERATOR, DENUMERTOR} ) -> GCD = gcd(NUMERATOR,DENUMERTOR),
  reduction2({round(NUMERATOR/GCD), round(DENUMERTOR/GCD)}).

